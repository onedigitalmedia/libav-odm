# Libav-odm README #

Please see the README file for the original Libav readme.

## building on Ubuntu ##

Install some of the libs automatically:
    apt-get build-dep libav

Here are some hints if you have a hard time building on Ubuntu:

    apt-get purge libav-tools libavfilter2 libavdevice53
    apt-get install build-essential nasm yasm pkg-config zlib1g-dev

To get a wider range of supported codecs, some of which are under incompatible licenses:

    apt-get install libmp3lame-dev libspeex-dev libtheora-dev libbz2-dev libvpx-dev libx264-dev libvorbis-dev zlib1g-dev
    apt-get install libgif-dev libglib2.0-dev libjpeg-turbo8-dev libjpeg8-dev libjpeg-dev liblzma-dev libimlib2-dev libsdl1.2-dev x11proto-video-dev libxv-dev libpng12-dev libfreetype6-dev
    ./configure --enable-nonfree --enable-gpl --disable-debug --enable-libvpx --enable-libx264 --enable-libvorbis --enable-libspeex --enable-libmp3lame --enable-libtheora --enable-x11grab
